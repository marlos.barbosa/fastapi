from pydantic import BaseModel, Field
from .generic import GenericModel
from uuid import UUID
from typing import List
from schemas.professor import ProfessorData

class ClassData(GenericModel):
    name: str = Field(title='Name of class')
    professor_id: UUID = Field(title='ID of class professor')
    code: str | None = Field(title='Code of class')
    description: str | None = Field(title='Description of class')

class Class(ClassData):
    id: UUID =Field(title='ID of class')

class ClassOut(GenericModel):
    id: UUID = Field(title='ID of course')
    name: str = Field(title='Name of class')
    professor: ProfessorData = Field(title='Data of professor')
    code: str | None = Field(title='Code of class')
    description: str | None = Field(title='Description of class')

class ClassList(BaseModel):
    __root__: List[ClassOut]

    class Config:
        arbitrary_types_allowed = True
        schema_extra = {
            "title": 'List of classes'
        }


