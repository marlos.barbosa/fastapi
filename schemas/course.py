from pydantic import BaseModel, Field
from .generic import GenericModel
from uuid import UUID
from datetime import date
from typing import List
from schemas.professor import ProfessorData

class CourseData(GenericModel):
    name: str = Field(title='Name of course')
    coordinator_id: UUID = Field(title='ID of course coordinator')
    creation_date: date | None = Field(default=None, title='Creation date of course')
    building_name: str| None = Field(title='Building name of course')

class Course(CourseData):
    id: UUID = Field(title='ID of course')

class CourseOut(GenericModel):
    id: UUID = Field(title='ID of course')
    name: str = Field(title='Name of course')
    coordinator: ProfessorData = Field(title='Data of coordinator')
    creation_date: str | None = Field(default=None, title='Creation date of course')
    building_name: str| None = Field(title='Building name')

class CourseList(BaseModel):
    __root__: List[CourseOut]

    class Config:
        arbitrary_types_allowed = True
        schema_extra = {
            "title": 'List of courses'
        }