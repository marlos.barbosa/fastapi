from pydantic import BaseModel, Field
from .generic import GenericModel
from uuid import UUID
from datetime import date
from typing import List
from schemas.course import Course, CourseOut
from schemas.discipline import Class

class StudentData(GenericModel):    
    name: str = Field(title='Name of student')
    course_id: UUID = Field(title='ID of student course')
    birth_date: date | None = Field(default=None, title='Birth date of student')
    cpf: str = Field(title="CPF of student", regex='[0-9]{3}[\.]?[0-9]{3}[\.]?[0-9]{3}[\-]?[0-9]{2}')
    rg: str | None = Field(title='RG of student')
    dispatching_agency: str | None = Field(title='Dispatching agency of student RG')

class StudentData1(GenericModel):    
    name: str = Field(title='Name of student')
    birth_date: date | None = Field(default=None, title='Birth date of student')
    cpf: str = Field(title="CPF of student", regex='[0-9]{3}[\.]?[0-9]{3}[\.]?[0-9]{3}[\-]?[0-9]{2}')
    rg: str | None = Field(title='RG of student')
    dispatching_agency: str | None = Field(title='Dispatching agency of student RG')

class Student(StudentData):
    id: UUID = Field(title='ID of student')
    course: Course | None = Field(title='Course of the student')

class StudentOut(GenericModel):
    id: UUID = Field(title='ID of student')
    course: CourseOut | None = Field(title='Course of the student')
    name: str = Field(title='Name of student')
    birth_date: date | None = Field(default=None, title='Birth date of student')
    cpf: str = Field(title="CPF of student", regex='[0-9]{3}\.?[0-9]{3}\.?[0-9]{3}\-?[0-9]{2}')
    rg: str | None = Field(title='RG of student')
    dispatching_agency: str | None = Field(title='Dispatching agency of student RG')

class StudentList(BaseModel):
    __root__: List[StudentOut]

    class Config:
        arbitrary_types_allowed = True
        schema_extra = {
            "title": 'List of students'
        }

class StudentList1(BaseModel):
    __root__: List[StudentData1]

    class Config:
        arbitrary_types_allowed = True
        schema_extra = {
            "title": 'List of students from course'
        }