from pydantic import BaseModel, Field
from .generic import GenericModel
from uuid import UUID
from typing import List

class ProfessorData(GenericModel):
    cpf: str = Field(title="CPF of professor", regex='[0-9]{3}\.?[0-9]{3}\.?[0-9]{3}\-?[0-9]{2}')
    name: str = Field(title='Name of professor')
    academic_degree: str = Field(title='Academic degree of professor')


class Professor(ProfessorData):
    id: UUID = Field(title='ID of professor')

class ProfessorOut(GenericModel):
    id: UUID = Field(title='ID of professor')
    cpf: str = Field(title="CPF of professor", regex='[0-9]{3}\.?[0-9]{3}\.?[0-9]{3}\-?[0-9]{2}')
    name: str = Field(title='Name of professor')
    academic_degree: str = Field(title='Academic degree of professor')


class ProfessorList(BaseModel):
    __root__: List[Professor]

    class Config:
        arbitrary_types_allowed = True
        schema_extra = {
            "title": 'List of professors'
        }