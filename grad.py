from fastapi import FastAPI, HTTPException
import uvicorn
from uuid import UUID, uuid4
import sqlite3
from schemas.student import StudentData, StudentList, StudentOut, StudentList1
from schemas.professor import ProfessorList, ProfessorData, ProfessorOut
from schemas.course import CourseList, CourseOut, CourseData
from schemas.discipline import ClassList, ClassData, ClassOut


app = FastAPI(title="PROGRAD API")

# Tags
professor_tag = 'Professors'
student_tag = 'Students'
enrollment_tag = 'Enrollments'
course_tag = 'Courses'
class_tag = 'Classes'


# Endpoint: Read all professors data
@app.get(
    '/professors',
    summary='Fetch all professors data',
    tags=[professor_tag],
    response_model=ProfessorList
)
def get_professors():
    connection = sqlite3.connect("./database.db")
    client = connection.cursor()
    professor_list = []
    client.execute(f'SELECT * FROM PROFESSORES')
    professor = client.fetchall()
    for i in range (0,len(professor)):
        my_row = {
            'cpf': None,
            'name': None,
            'academic_degree': None,
            'id': None
        }
        my_row['cpf'] = professor[i][1]
        my_row['name'] = professor[i][2]
        my_row['academic_degree'] = professor[i][3]
        my_row['id'] = professor[i][0]
        professor_list.append(my_row)
    return professor_list

#Endpoint: Add new professor
@app.post(
    '/professor',
    summary='Add new professor',
    tags=[professor_tag]
)
def post_course(new_professor: ProfessorData):
    connection = sqlite3.connect("./database.db")
    client = connection.cursor()
    professor_id: UUID = uuid4()
    client.execute(f'INSERT INTO PROFESSORES (professor_id, CPF, professor_name, professor_academic_degree) VALUES("{professor_id}","{new_professor.cpf}","{new_professor.name}","{new_professor.academic_degree}")')
    connection.commit()
    return {"response": "Professor adicionado!"}

#Endpoint: Read one professor data
@app.get(
    '/professores/{professor_id}',
    summary='Fetch one professor data',
    tags=[professor_tag],
    response_model=ProfessorOut
)
def get_course(professor_id: UUID):
    connection = sqlite3.connect("./database.db")
    client = connection.cursor()
    client.execute(f'SELECT * FROM PROFESSORES WHERE professor_id ="{professor_id}"')
    professor = client.fetchone()
    specific_professor = ProfessorOut(id = professor[0],cpf=professor[1],name=professor[2],academic_degree=professor[3])
    return specific_professor

#Endpoint: Read all courses data
@app.get(
    '/courses',
    summary='Fetch all courses data',
    tags=[course_tag],
    response_model=CourseList
)
def get_courses():
    connection = sqlite3.connect("./database.db")
    client = connection.cursor()
    course_list = []
    client.execute(f'SELECT * FROM CURSOS')
    curso = client.fetchall()
    for i in range (0,len(curso)):
        client.execute(f'SELECT CPF,professor_name,professor_academic_degree FROM PROFESSORES WHERE professor_id ="{curso[i][1]}"')
        coordinator_data = client.fetchone()
        aux = ProfessorData(cpf = coordinator_data[0], name = coordinator_data[1], academic_degree=coordinator_data[2])
        my_row = {
            'id': None,
            'coordinator': None,
            'name': None,
            'creationDate': None,
            'building_name': None
        }
        my_row['id'] = curso[i][0]
        my_row['coordinator'] = aux
        my_row['name'] = curso[i][2]
        my_row['creationDate'] = curso[i][3]
        my_row['building_name'] = curso[i][4]
        course_list.append(my_row)
    return course_list

#Endpoint: Read one course
@app.get(
    '/courses/{course_id}',
    summary='Fetch one course data',
    tags=[course_tag],
    response_model=CourseOut
)
def get_course(course_id: UUID):
    connection = sqlite3.connect("./database.db")
    client = connection.cursor()
    client.execute(f'SELECT * FROM CURSOS WHERE course_id ="{course_id}"')
    course = client.fetchone()
    client.execute(f'SELECT CPF,professor_name,professor_academic_degree FROM PROFESSORES WHERE professor_id ="{course[1]}"')
    coordinator_data = client.fetchone()
    aux = ProfessorData(cpf = coordinator_data[0], name = coordinator_data[1], academic_degree=coordinator_data[2])
    specific_course = CourseOut(id = course[0],name=course[2],coordinator = aux,creation_date=course[3],building_name=course[4])
    return specific_course

#Endpoint: Add new course
@app.post(
    '/courses',
    summary='Add new course',
    tags=[course_tag]
)
def post_course(new_course: CourseData):
    connection = sqlite3.connect("./database.db")
    client = connection.cursor()
    course_id: UUID = uuid4()
    client.execute(f'SELECT * FROM PROFESSORES WHERE professor_id ="{new_course.coordinator_id}"')
    checking_existence = client.fetchone()
    if checking_existence==None:
        return {"response": "Não existe um professor com esse ID!"}
    client.execute(f'INSERT INTO CURSOS (course_id, coordinator_id, course_name, creation_date, building_name) VALUES("{course_id}","{new_course.coordinator_id}","{new_course.name}","{new_course.creation_date}","{new_course.building_name}")')
    connection.commit()
    return {"response": "Curso adicionado!"}


#Endpoint: Read all classes data
@app.get(
    '/classes',
    summary='Fetch all classes data',
    tags=[class_tag],
    response_model=ClassList
)
def get_classes():
    connection = sqlite3.connect("./database.db")
    client = connection.cursor()
    course_list = []
    client.execute(f'SELECT * FROM DISCIPLINAS')
    discipline = client.fetchall() 
    for i in range (0,len(discipline)):
        client.execute(f'SELECT CPF,professor_name,professor_academic_degree FROM PROFESSORES WHERE professor_id ="{discipline[i][1]}"')
        professor_data = client.fetchone()
        aux = ProfessorData(cpf = professor_data[0], name = professor_data[1], academic_degree=professor_data[2])
        my_row = {
            'id': None,
            'name': None,
            'professor': None,            
            'code': None,
            'description': None
        }
        my_row['id'] = discipline[i][0]
        my_row['professor'] = aux
        my_row['name'] = discipline[i][2]
        my_row['code'] = discipline[i][3]
        my_row['description'] = discipline[i][4]
        course_list.append(my_row)
    return course_list

#Endpoint: Read one class data
@app.get(
    '/classes/{class_id}',
    summary='Fetch one class data',
    tags=[class_tag],
    response_model=ClassOut
)
def get_course(subject_id: UUID):
    connection = sqlite3.connect("./database.db")
    client = connection.cursor()
    client.execute(f'SELECT * FROM DISCIPLINAS WHERE subject_id ="{subject_id}"')
    discipline = client.fetchone()
    client.execute(f'SELECT CPF,professor_name,professor_academic_degree FROM PROFESSORES WHERE professor_id ="{discipline[1]}"')
    professor_data = client.fetchone()
    aux = ProfessorData(cpf = professor_data[0], name = professor_data[1], academic_degree=professor_data[2])
    specific_class = ClassOut(id = discipline[0],name=discipline[2],professor = aux,code=discipline[3],description=discipline[4])
    return specific_class

#Endpoint: Add new class
@app.post(
    '/classes',
    summary='Add new class',
    tags=[class_tag]
)
def post_course(new_class: ClassData):
    connection = sqlite3.connect("./database.db")
    client = connection.cursor()
    class_id: UUID = uuid4()
    client.execute(f'SELECT * FROM PROFESSORES WHERE professor_id ="{new_class.professor_id}"')
    checking_existence = client.fetchone()
    if checking_existence==None:
        return {"response": "Não existe um professor com esse ID!"}
    client.execute(f'INSERT INTO DISCIPLINAS (subject_id, professor_id, subject_name, code, description) VALUES("{class_id}","{new_class.professor_id}","{new_class.name}","{new_class.code}","{new_class.description}")')
    connection.commit()
    return {"response": "Disciplina adicionada!"}



#Endpoint: Add new student
@app.post(
    '/students',
    summary='Add new student',
    tags=[student_tag]
)
def post_student(new_student: StudentData):
    connection = sqlite3.connect("./database.db")
    client = connection.cursor()
    student_id: UUID = uuid4()
    client.execute(f'SELECT * FROM CURSOS WHERE course_id ="{new_student.course_id}"')
    checking_existence = client.fetchone()
    if checking_existence==None:
        return {"response": "Não existe um curso com esse ID!"}
    client.execute(f'INSERT INTO ALUNOS (student_id, course_id, CPF, student_name, birthday_date, RG, dispatching_agency) VALUES("{student_id}","{new_student.course_id}","{new_student.cpf}","{new_student.name}","{new_student.birth_date}","{new_student.rg}","{new_student.dispatching_agency}")')
    connection.commit()
    return {"response": "Estudante adicionado!"}

#Endpoint: Read all student data
@app.get(
    '/students',
    summary='Fetch all students data',
    tags=[student_tag],
    response_model=StudentList
)
def get_students():
    connection = sqlite3.connect("./database.db")
    client = connection.cursor()
    student_list = []
    client.execute(f'SELECT * FROM ALUNOS')
    student = client.fetchall() 
    for i in range (0,len(student)):
        client.execute(f'SELECT coordinator_id,course_name,creation_date,building_name FROM CURSOS WHERE course_id ="{student[i][1]}"')
        course_data = client.fetchone()
        client.execute(f'SELECT CPF,professor_name,professor_academic_degree FROM PROFESSORES WHERE professor_id ="{course_data[0]}"')
        professor_data = client.fetchone()
        aux1 = ProfessorData(cpf = professor_data[0], name = professor_data[1], academic_degree=professor_data[2])
        aux2 = CourseOut(id = course_data[0],name = course_data[1], coordinator= aux1, creation_date=course_data[2], building_name=course_data[3])
        my_row = {
            'id': None,
            'course': None,
            'name': None,
            'birth_date': None,
            'cpf': None,
            'rg': None,
            'dispatching_agency': None           
        }
        my_row['id'] = student[i][0]
        my_row['course'] = aux2
        my_row['name'] = student[i][3]
        my_row['birth_date'] = student[i][4]
        my_row['cpf'] = student[i][2]
        my_row['rg'] = student[i][5]
        my_row['dispatching_agency'] = student[i][6]
        student_list.append(my_row)
    return student_list

#Endpoint: Read one student data
@app.get(
    '/students/{student_id}',
    summary='Fetch one student data',
    tags=[student_tag],
    response_model=StudentOut
)
def get_student(student_id: UUID):
    connection = sqlite3.connect("./database.db")
    client = connection.cursor()
    client.execute(f'SELECT * FROM ALUNOS WHERE student_id ="{student_id}"')
    student = client.fetchone()
    client.execute(f'SELECT coordinator_id,course_name,creation_date,building_name FROM CURSOS WHERE course_id ="{student[1]}"')
    course_data = client.fetchone()
    client.execute(f'SELECT CPF,professor_name,professor_academic_degree FROM PROFESSORES WHERE professor_id ="{course_data[0]}"')
    professor_data = client.fetchone()
    aux1 = ProfessorData(cpf = professor_data[0], name = professor_data[1], academic_degree=professor_data[2])
    aux2 = CourseOut(id = course_data[0],name = course_data[1], coordinator= aux1, creation_date=course_data[2], building_name=course_data[3])
    specific_student = StudentOut(id = student[0],course = aux2, name = student[3],birth_date = student[4], cpf = student[2],rg = student[5],dispatching_agency=student[6])
    return specific_student

#Endpoint: Update student course
@app.put(
    '/students/{student_id}/courses/{course_id}',
    summary = 'Set or change student course',
    tags = [student_tag]
)
def update_student_course(student_id: UUID,course_id: UUID):
    connection = sqlite3.connect("./database.db")
    client = connection.cursor()
    client.execute(f'SELECT * FROM ALUNOS WHERE student_id ="{student_id}"')
    checking_existence1 = client.fetchone()
    if checking_existence1==None:
        return {"response": "Não existe um aluno com esse ID!"}
    client.execute(f'SELECT * FROM CURSOS WHERE course_id ="{course_id}"')
    checking_existence2 = client.fetchone()
    if checking_existence2==None:
        return {"response": "Não existe um curso com esse ID!"}
    client.execute(f'UPDATE ALUNOS SET course_id = "{course_id}" WHERE student_id = "{student_id}"')
    connection.commit()
    return {"response": "Curso atualizado!"}

#Endpoint: Update course coordinator
@app.put(
    '/courses/{course_id}/coordinator/{professor_id}',
    summary = 'Set or change course coordinator',
    tags = [course_tag]
)
def update_course_coordinator(course_id: UUID,coordinator_id: UUID):
    connection = sqlite3.connect("./database.db")
    client = connection.cursor()
    client.execute(f'SELECT * FROM CURSOS WHERE course_id ="{course_id}"')
    checking_existence1 = client.fetchone()
    if checking_existence1==None:
        return {"response": "Não existe um curso com esse ID!"}
    client.execute(f'SELECT * FROM PROFESSORES WHERE professor_id ="{coordinator_id}"')
    checking_existence2 = client.fetchone()
    if checking_existence2==None:
        return {"response": "Não existe um professor com esse ID!"}
    client.execute(f'UPDATE CURSOS SET coordinator_id = "{coordinator_id}" WHERE course_id = "{course_id}"')
    connection.commit()
    return {"response": "Coordenador atualizado!"}

#Endpoint: Update class professor
@app.put(
    '/classes/{class_id}/professor/{professor_id}',
    summary = 'Set or change class professor',
    tags = [class_tag]
)
def update_class_professor(subject_id: UUID,professor_id: UUID):
    connection = sqlite3.connect("./database.db")
    client = connection.cursor()
    client.execute(f'SELECT * FROM DISCIPLINAS WHERE course_id ="{subject_id}"')
    checking_existence1 = client.fetchone()
    if checking_existence1==None:
        return {"response": "Não existe ums disciplina com esse ID!"}
    client.execute(f'SELECT * FROM PROFESSORES WHERE professor_id ="{professor_id}"')
    checking_existence2 = client.fetchone()
    if checking_existence2==None:
        return {"response": "Não existe um professor com esse ID!"}
    client.execute(f'UPDATE DISCIPLINAS SET professor_id = "{professor_id}" WHERE subject_id = "{subject_id}"')
    connection.commit()
    return {"response": "Professor atualizado!"}

#Endpoint: Read all students from a course
@app.get(
    '/course/all_students',
    summary='Fetch all course students',
    tags=[course_tag],
    response_model=StudentList1
)
def get_students_course(course_id:UUID):
    connection = sqlite3.connect("./database.db")
    client = connection.cursor()
    student_list = []
    client.execute(f'SELECT * FROM ALUNOS WHERE course_id="{course_id}"')
    student = client.fetchall()
    for i in range (0,len(student)):
        my_row = {
            'name': None,
            'birth_date': None,
            'cpf': None,
            'rg': None,
            'dispatching_agency': None
        }
        my_row['name'] = student[i][2]
        my_row['birth_date'] = student[i][4]
        my_row['cpf'] = student[i][2]
        my_row['rg'] = student[i][5]
        my_row['dispatching_agency'] = student[i][6]
        student_list.append(my_row)
    return student_list

#Endpoint: Add new enrollment
@app.post(
    '/enrollment',
    summary='Add new enrollment',
    tags=[enrollment_tag]
)
def post_enrollment(student_id: UUID,subject_id: UUID):
    connection = sqlite3.connect("./database.db")
    client = connection.cursor()
    client.execute(f'SELECT * FROM ALUNOS WHERE student_id ="{student_id}"')
    checking_existence1 = client.fetchone()
    if checking_existence1==None:
        return {"response": "Não existe um aluno com esse ID!"}
    client.execute(f'SELECT * FROM DISCIPLINAS WHERE subject_id ="{subject_id}"')
    checking_existence2 = client.fetchone()
    if checking_existence2==None:
        return {"response": "Não existe uma disciplina com esse ID!"}
    client.execute(f'INSERT INTO ALUNOS_DISCIPLINAS (student_id,subject_id) VALUES("{student_id}","{subject_id}")')
    connection.commit()
    return {"response": "Matrícula realizada!"}

#Endpoint: Delete enrollment
@app.delete(
    '/enrollment',
    summary='Delete enrollment',
    tags=[enrollment_tag]

)
def post_enrollment(student_id: UUID,subject_id: UUID):
    connection = sqlite3.connect("./database.db")
    client = connection.cursor()
    client.execute(f'SELECT * FROM ALUNOS_DISCIPLINAS WHERE student_id ="{student_id}" AND subject_id = "{subject_id}"')
    checking_existence = client.fetchone()
    if checking_existence==None:
        return {"response": "Essa matrícula não existe!"}
    client.execute(f'DELETE FROM ALUNOS_DISCIPLINAS WHERE student_id ="{student_id}" AND subject_id = "{subject_id}"')
    connection.commit()
    return {"response": "Matrícula removida!"}

@app.get(
    '/enrollment/students',
    summary='Fetch all subject students',
    tags=[enrollment_tag],
    response_model=StudentList
    )
def get_subject_students(subject_id:UUID):
    connection = sqlite3.connect("./database.db")
    client = connection.cursor()
    student_list = []
    preliminar_list = []
    client.execute(f'SELECT student_id FROM ALUNOS_DISCIPLINAS WHERE subject_id="{subject_id}"')
    student_id = client.fetchall() 
    for i in range(0, len(student_id)):
        client.execute(f'SELECT * FROM ALUNOS WHERE student_id="{student_id[i][0]}"')
        student = client.fetchone()
        preliminar_list.append(student)
    for j in range (0,len(student_id)):
        client.execute(f'SELECT coordinator_id,course_name,creation_date,building_name FROM CURSOS WHERE course_id ="{preliminar_list[j][1]}"')
        course_data = client.fetchone()
        client.execute(f'SELECT CPF,professor_name,professor_academic_degree FROM PROFESSORES WHERE professor_id ="{course_data[0]}"')
        professor_data = client.fetchone()
        aux1 = ProfessorData(cpf = professor_data[0], name = professor_data[1], academic_degree=professor_data[2])
        aux2 = CourseOut(id = course_data[0],name = course_data[1], coordinator= aux1, creation_date=course_data[2], building_name=course_data[3])
        my_row = {
            'id': None,
            'course': None,
            'name': None,
            'birth_date': None,
            'cpf': None,
            'rg': None,
            'dispatching_agency': None           
        }
        my_row['id'] = preliminar_list[j][0]
        my_row['course'] = aux2
        my_row['name'] = preliminar_list[j][3]
        my_row['birth_date'] = preliminar_list[j][4]
        my_row['cpf'] = preliminar_list[j][2]
        my_row['rg'] = preliminar_list[j][5]
        my_row['dispatching_agency'] = preliminar_list[j][6]
        student_list.append(my_row)    
    return student_list

if __name__ == '__main__':
    uvicorn.run(app='grad:app', port=3000, reload=True)
